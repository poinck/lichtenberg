\centerline{ }

\vspace{4cm}

\centerline{\Huge \textbf{Spielregeln}}
\vspace{0.8cm}

\centerline{\includegraphics[width=\textwidth,keepaspectratio]{./gl_logo.png}}

\vspace{0.8cm}

\centerline{\large Umweltbüro Lichtenberg, 2022--2024, CC-BY-SA}
\vspace{0.1cm}
\centerline{\large \url{http://umweltbuero-lichtenberg.de/}}

\rhead{}
\rfoot{}
\normalsize

\newpage

\pagenumbering{roman}
\setcounter{page}{2}
\renewcommand{\contentsname}{Inhalt}
\tableofcontents
\rfoot{\small{\textit{\thepage}}}

\rhead{\small{\textit{Inhalt}}}

\newpage

\pagenumbering{arabic}
\setcounter{page}{3}
\lhead{\small{\textit{GRÜN-Lichtenberg, Spielregeln}}}
\rhead{\small{\textit{\leftmark}}}

# Einleitung und Hintergrund

„GRÜN-Lichtenberg“ ist ein kooperatives Umweltbildungspiel für Lichtenberg. Ziel ist es den Wandel hin zu einem grüneren Lichtenberg zu erreichen. Erhalte oder steigere die Biodiversität trotz neuer Bedarfe durch Bevölkerungswachstum und Zuzug, sorge für eine stabile Grundwasserbilanz und reduziere die  thermische Belastung.

Speile in maximal 6 Runden ab dem Jahr 2020 bis 2095 oder sobald der Marker für den Wandel auf „GAME OVER“ steht. In diesem Fall wäre das Spiel für alle verloren. Bleibt die Position des Markers hingegen nach 6 Runden im grünen Bereich wurde das Spiel gewonnen und Lichtenberg kann in eine grüne Zukunft blicken.

Viel Spaß!

# Spielmaterialien

<!--
Das Brettspiel besteht aus folgenden Teilen *(Tabelle \ref{mat})*:

\begin{table}[h!]
  \begin{center}
    \caption{Spielmaterialien. $^{a}$Während der Prototypphase sind einige Angaben nur eine Schätzung.}
    \label{mat}
    \small
    \begin{tabular}{rl} % <-- alignments
      \textbf{Menge} & \textbf{Beschreibung} \\
      \hline
      1 & Haupt-Spielbrett \\
      5 & Stadtteil-Spielpläne \\
      ca. 40$^{a}$ & Ereigniskarten (Krisen in rot und normale Ereignisse grün) \\
      ca. 90$^{a}$ & Projektkarten \\
      ca. 20$^{a}$ & Effektkarten für Grundwasser, thermische Belastung und Biodiversität \\
       & mit unterschiedlichen Zahlenwerten von -3 bis +3 \\
      ca. 20$^{a}$ & rosa oder pinke Mobilität-Spielsteine (0,7 cm) \\
      ca. 60$^{a}$ & schwarze Wohnviertel-Spielsteine (0,7 cm) \\
      ca. 60$^{a}$ & graue Spielsteine für nicht sanierte Wohnviertel (0,7 cm) \\
      ca. 60$^{a}$ & grüne Vegetation-Spielsteine (0,7 cm) \\
      ca. 20$^{a}$ & gelbe Energie-Spielsteine (0,7 cm) \\
      1 & schwarzer Spielstein für das Level der Entsiegelung (1 cm) \\
      1 & grüner Spielstein für das Level der Vegetation (1 cm) \\
      1 & weißer oder blauer Spielstein für das Grundwasserlevel (1 cm) \\
      1 & roter Spielstein für das Level der thermischen Belastung (1 cm) \\
      1 & gelber Spielstein für das Biodiversitätslevel (1 cm) \\
      1 & naturfarbener großer Spielstein für das Level des Wandels \\
      ca. 25 & runde Spielsteine in unterschiedlichen Farben für die Markierungen des \\
       & Planungsbudgets, Abzahl der bebauten Flächen oder Flächen mit Vegetation \\
    \end{tabular}
  \end{center}
\end{table}
-->

## Spielbretter, -steine und -figuren \label{bretter}

Das Spiel kann mit **2** bis **5** Spieler:innen gespielt werden. Für jede Spieler:in gibt es jeweils ein Spielplan mit 2 bis 3 Stadtteilen des Berliner Bezirks Lichtenberg *(Abb.\ \ref{brett})*. Die Spielpläne können frei gewählt werden, mit Ausnahme des Plans mit den Stadtteilen Malchow, Wartenberg, Falkenberg und Neu-Hohenschönhausen, welches bei jedem Spiel von einer der Spieler:innen übernommen werden sollte[^mwfh]. Da jeder Spielplan unterschiedliche Ausgangsbedingungen hat, kommt es beim grünen Wandel Lichtenbergs auf Zusammenarbeit an.

[^mwfh]: Wird dieser Spielplan nicht verwendet, dann wird es unter Umständen schwerer den Wandel zu schaffen. Probiert es gerne aus, wenn ihr **\textsf{GRÜN-Lichtenberg}** schon ein paar mal gewonnen habt.

### Stadtteile \label{stadtteile}

\pic{./spielplan_b1.png}{\textsf{\textbf{NEU-Lichtenberg}}-Stadtteil-Spielplan mit Startaufstellung für eine Spieler:in.\label{brett}}

Jede Spieler:in hat seine eigenen Spielfelder (*Abb.\ \ref{brett}*, mit aufgebauter Startaufstellung). Ein Feld gilt als versiegelt, wenn sich darauf Bebauung in Form von Wohnfläche *(schwarz)* oder Erzeugung regenerativer Energieen *(gelb)* befinden. Befindet sich zusätzlich noch Vegetation auf dem gleichen Feld, dann gilt dies trotzdem als versiegelt. Felder mit Mobilität (z.\ B. Öffentlicher Personennahverkehr, Auto- und Fahrrad-Sharing) wirken sich positiv auf die Entsiegelung aus, weil in diesem Spiel angenommen wird, dass sich dadurch Versiegelung in Form von geteilter Verkehrsinfrastruktur verhindern lässt. Diese werden also während der Wertung der Entsiegelung abgezogen, auch wenn sie sich zusammen mit Wohnfläche oder Energie auf einem Feld befinden. Auf jedem kleinen Spielfeld (grau-lila, 1,5 cm) können maximal 3 Spielsteine platziert werden, auf den größeren (grau-grün, 2 cm) bis zu 4 Spielsteine.

Für alle Stadtteil-Spielpläne ist die Startaufstellung mit entsprechend farbig markierten Symbolen auf den Spielfeldern dargestellt.


### Hauptspielbrett \label{trans}

Das Haupt-Spielbrett dient dazu euren Fortschritt während des Spiels festzustellen *(Abb.\ \ref{trans1})*. Das Spielbrett ist in drei Teile aufgeteilt (von links nach rechts): aktuelle Spielrunde, Wandel und Zählhilfe für Flächen und Bebauung.

Die aktuelle Spielrunde wird mit einem naturfarbenen Würfel angezeigt. In der ersten Runde steht der Würfel auf **$\textsf{1}$**, zeigt somit das Jahr 2020 und eine Bevölkerung von **\textsf{7}** an.

\pic{./hauptspielbrett.png}{\textsf{\textbf{GRÜN-Lichtenberg}}-Haupt-Spielbrett.\label{trans1}}

<!--
sodass am aber nach Abzug noch mindestens 5 Planungsressourcen übrig bleiben (Beispiel: Runde 5: Bevölkerung 12, Wohnflächen 10, Planungsbudget wird um 2 auf 8 reduziert).
-->

In der ersten Reihe des mittleren Teils „Wandel“ wird euer aktuelles Level des Wandels ebenfalls  mit einem großen naturfarbenen Würfel markiert.

In den 5 Reihen darunter werden die Level der 5 Kategorien (Entsiegelung, Vegetation, Grundwasser, Thermische Belastung und Biodiversität) dargestellt. Für jede Kategorie wird jeweils ein anders farbiger Spielstein verwendet um den aktuellen Status zu markieren. Welche Werte diese einnehmen wird während der Wertung mithilfe der auf den Spielplänen aufgedruckten Zusammenhänge bestimmt *(Kapitel\ \ref{wertung})*. Wo genau der Würfel zum Markieren des aktuellen Levels (erste Reihe) liegt, hängt vom gewählten Schwierigkeitsgrad und von den Werten der 5 Kategorien ab:

- **sehr schwer:** niedrigste Position einer der 5 Kategorien bestimmt das Level des Wandels.
- **schwer:** 2. niedrigste Position bestimmt den Wandel.
- **mittel (Empfehlung):** 3. niedrigste Position.
- **leicht:** 4. niedrigste.
- **sehr leicht:** 5. niedrigste (Sandkastenstufe).

Rechts daneben befindet sich zum schnelleren Zählen in der Wertungphase nur die Zählhilfe für die Flächen mit Vegetation und Wohnviertel sowie die Bebauung. Bei der Bebauung wird jeder Spielstein einzeln gezählt, bei den Flächen nur die Felder mit der entsprechenden Bebauung. Befinden sich auf einem Feld zwei oder mehr Spielsteine der selben Farbe gilt dies trotzdem nur als eine Fläche dieser Art. Zum Anzeigen der Anzahl werden die Runden Spielsteiner der jeweiligen Spieler:in verwendet.

Die letzte Reihe mit den Werten „+10“ wird dann mit einem runden Spielstein in der Fabre der Spieler:in besetzt, wenn es 10 oder mehr Flächen bzw. Bebauung einer bestimmten Art gibt.

## Ereigniskarten

Ereigniskarten bestimmen wiederkehrende zufällige oder einmalige Ereignisse, die den Spielverlauf in allen Spielphasen beeinflussen können. Je nach Level des Wandels werden unterschiedliche viele grüne und rote Ereigniskarten gezogen *(siehe Kapitel \ref{ereignisse})*.

\pic{./krisen_und_erfolge.png}{Rote Krisen und grüne Erfolge\label{eplus}}

<!--\pic{./ereignis_descr.png}{Rück- und Vorderseite einer Ereigniskarte\label{edescr}}-->

\hyphenation{Planungs-ressourcen}

*Abb.\ \ref{eplus}* zeigt Ereigniskarten, die sich negativ auf die Stadteilspielpläne oder positiv auf das Planungsbudgetauswirken können. Auswirkungen auf den Planungsaufwand oder die eigenen Planungsressourcen werden immer oben rechts blau unterlegt dargestellt und auf der Karte genauer beschrieben. Der Text erklärt jeweils in Worten die Symbole und nennt ggf. bestimmte Bedingungen die erfüllt sein müssen.

## Projektkarten

Projektkarten stehen für Projektvorhaben, die während der Planungsphase für **1** Planungsaufwand pro Karte behalten *(siehe Kapitel \ref{projekte})* und in der Umsetzungsphase zum aufgedruckten Planungsaufwand ausgespielt werden können *(siehe Kapitel \ref{umsetzung})*.

\pic{./projekte.png}{Projektkarten\label{p}}

<!--\pic{./projekt_descr.png}{Rück- und Vorderseite einer Projektkarte\label{pdescr}}-->

*Abb.\ \ref{p}* zeigt Projektkarten die es ermöglichen für Planungsressources neue Wohnviertel zu errichten, Windräder aufzustellen oder in Abhängigkeit von vorhandenen Feldern mit ausschließlich Vegetation, die Buidiversität zu steigern. Auch hier sind oben rechts blau unterlegt die jeweiligen Planungsaufwände darfestellt, die bei der Umsetzung aufgewendet werden müssen. Das Symbol in der Mitte deutet mit dem **\textsf{+}** an, dass auf dem Spielplan Spielsteine gesetzt werden oder vorhanden sein müssen (nicht ausgefüllt). Es gibt immer einen Text, der die Umsetzung beschreibt und ggf. Effekte die eintreten, wenn das Projekt umgesetzt wird *(siehe Kapitel\ \ref{effekte})*.

## Effektkarten \label{effekte}

In manchen Fällen gibt es bei Projekte oder Ereignissen zusätzliche Auswirkungen: Der Grundwasserspiegelspiegel könnte sinken, die thermische Belastung steigen oder die Biodiversität sinken. Diese sind mit dem entsprechenden Vorzeichen gekennzeichnet und auf den Karten näher beschrieben. Je nach Effekt werden kleinere Effektkarten auf die dafür vorgesehene Ablage auf dem Hauptspielbrett abgelegt. Ausnahmen sind auf den Karten entsprechend beschrieben. 

(TODO: Bild der Effektkarten einfügen)

\newpage

# Spielphasen (Spielablauf) \label{phasen}

Das Spiel läuft in mehreren Phasen ab, und beginnt mit der *Wertung* direkt nach der *Startaufstellung*, wobei der erste *Rundenwechsel* sowie *Wohnbedarf und Sanierung* zu Beginn ausgelassen werden. Die *Ereignis-*, *Planungs-* und *Umsetzungsphase* finden jede Runde in dieser Reihenfolge statt und schließen jeweils mit dem *Rundenwechsel*, *Wohnbedarf und Sanierung* sowie der *Wertung* ab.

## Startaufstellung

Die roten/grünen *Ereignis-* und *Projektkarten* werden verdeckt als 3 Stapel neben das Haupt-Spielbrett gelegt, welches wie in *Kapitel \ref{trans}* beschrieben, vorbereitet werden kann.

Alle Spieler:innen können nun die Startaufstellung mit Spielsteinen für Wohnflächen (schwarz), Mobilität (rosa), Energie (gelb) und Vegetation (grün) auf die ausgewählten Stadtteil-Spielplänen wie aufgedruckt besetzen. Bitte hier auch die Hinweise aus dem *Kapitel \ref{bretter}* beachten. 

Das Planungsbudget wird zu Beginn jeder Runde vor der Ereignisphase mit einem runden Spielstein in der Farbe der Spieler:in auf 10 gesetzt (siehe rechte Seite der Spielpläne).

## Rundenwechsel

Der Marker für die Planungsbudget aller Spieler:innen wird auf den Spielplänen wieder auf 10 gesetzt. 

Schiebe nun den den naturfarbenen Würfel auf die nächste Runde. Die Zahl in rot zeigt nun den neuen Wohnbedarf an. Jede Spieler:in überprüft nun die Anzahl der Wohnflächen. Ist die Anzahl kleiner, muss entsprechend Planungsbudget wieder abgezogen werden *(siehe Kapitel\ \ref{wohnbedarf})*.

## Wohnbedarf und Sanierungen \label{wohnbedarf}

Die Anzahl der Bevölkerung muss durch die Anzahl an Wohnflächen gedeckt sein (Wohnbedarf). Ist der Wohnbedarf zu Beginn einer Runde nicht erfüllt wird direkt nach dem *Rundenwechsel* und noch vor der *Planungsphase* das Planungsbudget um die gleiche Menge an fehlenden Wohnflächen für die entsprechende Spieler:in reduziert.

Bevor der aktuelle Wohnungsmangel ausgeglichen werden kann, muss noch der Bestand durch notwendige Sanierungen gesichert werden.

- entweder das Planungsbudget wird um **1** für jedes 4. Wohnvietel (schwarzer Spielstein) reduziert *oder*
- jedes 4. Wohnviertel wird mit einem grauen Stein als unbewohnbar markiert (ein schwarzer Spielstein wird durch einen grauen ersetzt).
- Hat eine Spieler:in weniger als 4 Wohnviertel, dann wird 1 Wohnviertel als sanierungsbedürftig markiert bzw. saniert.
- zusätzlich können überfällige Sanierungen (graue Spielsteine) nachgeholt werden. Hierfür werden für jedes unbewohnbare Wohnviertel wieder **1** Planungsbudget fällig und die grauen Steine dürfen entsprechend wieder durch schwarze Spielsteine ersetzt werden.

Ein Spielfeld mit unbewohnbaren Wohnungsvierteln kann nicht weiter ausgebaut werden, also auch nicht mit Vegetation, Mobilität oder Energieversorgung erweitert werden. Ferner verschärfen die fälligen Sanierungen den Wohnungsmangel, sodass diese nicht mehr als Wohnungsviertel gezählt werden können.

Nun wird Wohnungsmangel zur Hälfte durch die Entstehung von Notunterkünften ausgeglichen (ein Beispiel: Der Wohnbedarf ist 8, aber auf Plan 4 gibt es nur 5 Wohnviertel. Der Wohnungsmangel ist demzufolge 3, wovon 1.5 ausgeglichen werden muss. Es wird zu Ungunsten aufgerundet). Die betreffenden Spieler:innen müssen dann die Anzahl des Mangel mit je **einer** sanierungsbedüftigen Wohnbebauung (grauer Spielstein) auf einem seperaten freien Spielfeld ausgleichen. Im gezeigten Beispiel kommen als 2 graue Spielsteine auf zwei verschiedene freie Felder hinzu. Hat eine Spieler:in keine freien Flächen mehr, dann muss eine freie Fläche auf einem anderen Spielplan einer Mitspieler:in besetzt werden.

Folgende Extremfälle können entstehen:

- Stehen auf den Spielfeldern aller Spielpläne keine freien Felder mehr zur Verfügung, dann muss ein Feld ohne vorhandener Wohnbebauung freigeräumt werden.
- Ist der Wohnungsmangel so groß, dass eine Spieler:in kein Planungsbudget für die kommende Runde haben würde, dann muss stattdessen ein weiteres freies Feld mit einem regulären Wohnviertel (schwarzer Spielstein) bestetzt werden.
- Können keine Spielfelder ohne Wohnviertel freigeräumt werden, dann darf ein bereits bebautes Feld mit einem Wohnviertel oder Notunterkunft erweitert werden (bevorzugt auf dem Plan der betroffenen Spieler:in). Dabei gehen Ausbauten wie zusätzliche Mobilität, Vegetation oder regenerative Energien auf dem gleichen Feld verloren. Die maximale Anzahl an Bebauung pro Feld darf auch hier nicht überschritten werden.

Der Wohnbedarf steigt in jeder Runde außer der letzten (6.) an: Von 7 in der ersten Runde auf 8 in der zweiten. Dann auf 9 in der dritten, bis bis zum Höchststand 10 in der 4. und 5. Runde. In der sechsten und letzten Runde sinkt der Wohnbedarf wieder auf 9.

## Wertung \label{wertung}

Während der Wertung werden die Bebauung und Flächen auf den Spielplänen der Spieler:innen gezählt um das aktuelle Level des Wandels zu bestimmen. Auch zu Beginn des Spiels, vor der ersten Ereignisphase wird die Wertung einmal ausgeführt. Die Berechnungsregeln findet ihr auch auf jedem der 5 Stadtteil-Spielpläne. Dabei haben die rot bzw. grün markierte „2“ folgende Bedeutung:

- **rote Markierung** *(unter der Symbol)*: Die Flächen oder Bebauung der Spieler:in mit den zweitniedrigsten Anzahl wird für die Berechnung verwendet.
- **grüne Markierung** *(über der Symbol)*: Die Flächen oder Bebauung der Spieler:in mit den zweithöchsten Anzahl wird für die Berechnung verwendet.
- In der 2-Spieler:innen-Variante wird die Anzahl der Flächen oder Bebauung der Spieler:in mit der  niedigsten bzw. höchste Anzahl für die Berechnung verwendet, da sich sonst die Bedeutung umdrehen würde

Die Positionen der 5 Kategorien auf dem Haupt-Spielbrett werden nun wie folgt berechnet. 

- **Entsiegelung** $\textsf{(VS)}$ = Felder mit Vegetation \textsf{(V)} *plus* Felder mit Mobilität \textsf{(M)} *plus* Freifelder \textsf{(F)} *minus* Felder mit Wohnvierteln, Notunterkünften oder sanierungsbedürftigen Wohnvierteln \textsf{(W)} (schwarz und grau) *minus* Feldern mit regenerativen Energien \textsf{(F)}. Beachtet die Hinweise in *Kapitel\ \ref{stadtteile}* und *\ref{status}*.
- **Vegetation** $\textsf{(V)}$ = Vegetations-Spielsteine \textsf{(V)} ohne Freifelder (zweitniedrigste Anzahl auf einem der Spielpläne).
- **Grundwasser** $\textsf{(G)}$ = Wert aller Effekte die sich durch Projekt- oder Ereigniskarten auf die Grundwassertiefe ergeben haben.
- **Thermische Belastung** $\textsf{(T)}$ = Wohnviertel-Spielsteine, Notunterkünften oder sanierungsbedürftigen Wohnvierteln \textsf{(W)} (schwarz und grau) *minus* Mobilität-Spielsteine \textsf{(M)} *minus* Regenerative Engergie-Spielsteine \textsf{(RE)} *minus* Vegetation-Spielsteine \textsf{(M)}.
    - Ohne Freiflächen, da es hier um das Mikroklima in Wohnvierteln geht.
- **Biodiversiät** $\textsf{(B)}$ = Vegetation-Spielsteine \textsf{(V)} *plus* Freifelder \textsf{(F)} *plus* Effekte die sich durch Projekt- oder Ereigniskarten auf die Grundwassertiefe \textsf{(GT)} ergeben haben.

Achte beim Zählen darauf, ob Flächen oder Bebauung gezählt wird. Flächen erkennst du an einer Umrandung. Dabei werden mehrere Spielsteine auf einem Feld nur einmal gezählt. Eine Bebauung ist nicht umrandet und meint immer die tatsächliche Anzahl der Spielsteine einer Art auf dem gesamten Spielplan.

\hyphenation{Trans-formations-level}

Sind nun alle Positionen markiert kann der naturfarbene Würfel für den Wandel Lichtenbergs nach dem gewählten Schwierigkeitsgrades verschoben werden *(siehe Kapitel\ \ref{trans}* und *Abb.\ \ref{trans1})*. Verschiebt sich der Würfel auf „\textsf{GAME OVER}“, dann habt ihr den Wandel zu einem grünen Lichtenberg leider nicht geschafft. Vielleicht klappt es beim nächsten Mal. Andernfalls geht es weiter, sofern diese Wertung nicht schon am Ende der 6. Runde stattfindet. Ist das Ende erreicht könnt ihr direkt zum *Kapitel\ \ref{ende}* springen.
 
## Ereignisphase \label{ereignisse}

Ziehe 1 bis 3 Ereigniskarten und führe die beschriebenen Effekte sofort aus. Effekte gelten nur für die aktuelle Runde, es sei denn es wird auf einer Karte explizit anders beschrieben. Abhängig von der aktuellen Stufe des Wandels werden wie auf dem Hauptspielbrett wie folgt Karten gezogen:

- Level **\textsf{--}**: **2** *rote* Ereigniskarten
- Level **\textsf{0}**: **2** *rote* und **1** *grüne* Ereigniskarte
- Level **\textsf{1}**: **1** *rote* und **1** *grüne* Ereigniskarte
- Level **\textsf{+}**: **1** *grüne* Ereigniskarte
- Level **\textsf{++}**: **2** *grüne* Ereigniskarten

<!--
> `TODO: Beispiel eines Ereignisses erklären`
-->

## Planungsphase \label{projekte}

Alle Spieler:innen ziehen nacheinander verdeckt jeweils 4 Projektkarten und entscheiden möglichst ohne fremde Hilfe, welche davon sie für je eine Planungsressource in einer der folgenden Umsetzungsphasen behalten wollen. Nicht gewünschte Projekte werden verdeckt auf den Ablagestapel abgelegt. Es besteht die Möglichkeit, dass der Planungsprozess offen gespielt wird, wenn alle Spieler:innen damit einverstanden sind. Diskussionen verlängern unter Umständen das Spiel erhöhen aber auch die Wahrscheinlichkeit auf Erfolg. 

## Umsetzungsphase \label{umsetzung}

Die Umsetzungsphase ist die Hauptphase des Spiels. Jede Spieler:in kann maximal eine Aktion ausführen. Erst nach dem alle anderen Spieler:innen an der Reihe waren darf eine weitere Aktion der selben Spieler:in gespielt werden. Folgende Aktionen sind möglich:

**Abriss:** Es können jede Runde bis zu 2 Spielsteine für 2 Planungsressourcen abgerissen werden. Der Abriss kann auf mehrere Aktionen aufgeteilt werden, der Planungsaufwand wird aber nur einmal fällig. Werden weniger Spielsteine durch den Abriss vom Stadtteil-Spielbrett abgerissen müssen trotzdem 2 Planungsressourcen abgegeben werden.

**Projekttausch:** Projekte anderer Spieler:innen können für 2 Planungsressourcen pro Projekt jederzeit in der Umsetzungsphase getauscht werden. Spieler:innen können selbst entscheiden, wer dafür die zusätzlichen Planungskosten übernimmt (eine Aufteilung des Planungsaufwands ist ebenfalls möglich).

**Projekt umsetzen:** Wende für das gewünschte Projekt entsprechend das aufgedruckte Planungsbudget auf. Im Fall von Neubauprojekten müssen immer freie Felder besetzt werden, auch wenn noch Platz auf bereits bebauten Feldern ist, es sei denn das Projekt ermöglicht einen Ausbau direkt auf einem Feld mit vorhandener Bebauung.

<!--
> `TODO: Beispiel eines Projekts erklären`
-->

Nicht umgesetzte Projekte verbleiben auf der Hand der jeweiligen Spieler:in und können in einer späteren Runde gespielt werden. Nicht genutztes Planungsbudget verfällt am Ende der Runde und kann auch nicht anderen Spieler:innen zur Verfügung gestellt werden. Nutze dafür stattdessen die Möglichkeit des *Projekttauschs.*

## Spielende \label{ende}

Nach der letzten Runde oder nach einem fortzeitigen Ende nach der Wertung kann über das Erreichte diskutiert werden. Was hat gefehlt um den Wandel zu schaffen oder welche Ereignisse waren besondere Herausforderungen um Lichtenberg in eine grüne Zukunft zu bewegen? Anregungen oder Kritik könnt ihr gerne an das Umweltbüro Lichtenberg richten.

## 2-Spieler:innen-Variante

In der 2-Spieler:innen-Variante gibt es neben den bereits beschriebenen Änderungen noch folgendes zu beachten:

- Das Projekt 006 sollte aus dem Spiel genommen werden, da es nicht möglich ist, die genannten Bedingungen zu erfüllen.

\newpage

# Glossar

## Ereignisse

> `TODO`

## Projekte

> `TODO`

<!-- # Weiterführende Literatur -->
<!--
\rhead{\small{\textit{Weiterführende Literatur}}}
\addcontentsline{toc}{section}{Weiterführende Literatur}
\section*{Weiterführende Literatur}
-->


\small
