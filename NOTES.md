# Readme: NEU-Lichtenberg

Ein Umweltbildungsspiel für Lichtenberg


## Regeln

- Es können jede Runde bis zu 4 Spielsteine für 1 Planungsressource abgerissen werden (Aktion in der Umsetzungsphase).
- Projekte anderer Spieler:innen können für 1 Planungsressource pro Projekt jederzeit in der Umsetzungsphase getauscht werden. Spieler:innen können selbst entscheiden, wer dafür die Planung übernimmt (Aktion in der Umsetzungsphase für die Spieler:in, die den Tausch initiiert).

- Jede Spieler:in erhält 10 Planungsressourcen pro Runde

- *Ereigniskarten*, die aufgedeckt bleiben sollen, können nicht nochmal gezogen werden (kommen im Spiel nur einmal vor). Beim Testspiel entsprechend berücksichtigen! Es werden mindestens 24 rote und 18 grüne Ereigniskarten benötigt.
- Ausgespielte *Projektekarten* landen auf einem aufgedeckten persönlichen Ablagestapel (d.h. Karten können pro Spiel nur so oft gespielt werden, wie im Spiel vorhanden). Es werden mindestens 96 Projektkarten (6 Runden * 4 Spieler:innen * 4 Karten pro Runde) benötigt. Projektkarten, die die Transformationslevel direkt beeinflussen werden bei der Umsetzung seperat offen in die Mitte ausgelegt (Beispiele: Projekt 25 „Fahrradschnellweg“, 22 und 23).

**Spielfeldgrößen**

- Auf kleinen Spielfeldern (1,5 x 1,5 cm) können nur 3 Spielsteine platziert werden
- Auf großen Spielfeldern (2 x 2 cm) können bis zu 4 Spielsteine platziert werden

**Spielphasen**

- Ereignisphase: 1 bis 3 Ereigniskarten ziehen und sofort ausführen. Effekte gelten nur für die aktuelle Runde. Abhängig vom aktuellen Transformationslevel
    - Level -: **2** *rote* Ereigniskarten
    - Level 0: **2** *rote* und **1** *grüne* Ereigniskarte
    - Level +: **1** *grüne* Ereigniskarte
    - Level ++: **1** *grüne* Ereigniskarte
- Planungsphase: Spieler:innen ziehen je 4 Karten. Pro Projekt das später umgesetzt werden will, muss nun 1 Planungsressource abgegeben werden. Nicht geplante Projekte landen verdeckt auf dem Ablagestapel
- Umsetzungsphase: Projekte werden der Reihe nach umgesetzt und der aufgedruckte Bedarf an Planungsressourcen abgegeben. Statt Umsetzung von Projekten können auch o.g. Aktionen ausgeführt werden. Aktionen und Projekte können solange durchgeführt werden, wie Planungsressourcen vorhanden sind. Bevor eine Spieler:in ein weiteres Projekt umsetzen kann, müssen alle anderen Spieler:innen an der Reihe gewesen sein oder freiwillig ausgesetzt haben. Es darf auch nach dem Aussetzen wieder eine Aktion oder Projekt durchgeführt werden. Setzen alle Spieler:innen aus, dann ist die Runde ebenfalls zuende.
- Rundenwechsel: Planungsressourcen werden für alle wieder auf 10 aufgestockt.

**Energiebedarf:**

- Energiebedarf := (belegte Wohnfelder + belegte ÖPNV-Felder) / 4. Es wird immer aufgerundet!
    - Hochhäuser: So größer ein Mehrfamilienhaus, desto Energieeffizienter kann es gebaut werden
    - In Lichtenberg nicht selbst erzeugte Energie wird über Stromtrassen zugeführt. Der Mangel wird wie folgt ausgeglichen: Pro Defizit 1 Planungsressource weniger für alle Spieler:innen in der nächsten Runde.

- Folgende Flächennutzung benötigt Energie (Strom und/oder Wärme): alle Arten von Landwirtschaft ohne Streuobstwiesen, Wohnungen und ÖPNV

**Wohnbedarf:**

- der Wohnbedarf ist für jeden Stadtteil und Runde auf den jeweiligen Spielbrettern aufgedruckt
- kann durch Ereignisse (z.B. 17 Klimaflüchtlinge) verändert werden

**Transformation:**

- Versiegelung := Wohnungsfelder + Energieerzeugungsfelder - ÖPNV-Felder - Freifelder
    - z.B.: Ein Feld mit Wohnungs- und Vegetationsspielstein gilt als versiegelt
- Vegetation := Vegetationsspielsteine (OHNE Freifelder!)
- Grundwasser := Versiegelte Felder - Vegetationsfelder - Freifelder (ungenutzte Fläche)
- Thermische Belastung := Versiegelte Felder - Vegetationsspielsteine - Regenerative Engergie-Spielsteine
    - hier zählen auch Vegetationsspielsteine oder Regenerative Energie-Spielsteine auf versiegelten Feldern (z.B. in Kombination mit Wohnungen)
    - OHNE Freiflächen, da es hier um das Mikroklima geht (bis 2 Meter über den Boden)
- Biodiversiät := Vegetationsspielsteine + Freifelder

- Anpassungsmöglichkeiten des Schwierigkeitsgrades:
    - sehr schwer: höchster Marker bestimmt das Transformationslevel
    - schwer: 2. höchster Marker bestimmt das Transformationslevel
    - mittel (Empfehlung): 3. höchster ..
    - leicht: 4. höchster ..
    - sehr leicht: 5. höchster .. (Sandkastenstufe)

- Freifelder sind Brachen oder sonstige Flächen, die eine neutrale Ökosystemfunktion haben.

## Potentielle Probleme

- Herausforderung: Transformationslevel abhängig von Spieler:innen-Anzahl
    - Idee: Wenn es zu wenig Spieler:innen gibt, dann müssen einige Spieler:innen mehrere Stadtteile verantworten erhalten dann aber auch mehr Planungsressourcen (Stadteile mal 10).
- Nahrungsmittelbedarf wird nicht erfasst um Komplexität zu reduzieren, was aber dazu führen kann, dass das Spiel weniger glaubwürdig wird.

## TODOs

- Bevölkerungsmenge pro Runde für jeden Stadteil direkt auf die Stadteil-Spielbretter aufdrucken (Einsparen von zusätzlichen Spielsteinen)
- Transformationsspielbrett gemäß der neuen Abhängigkeiten überarbeiten
- Würfel für das Testspiel programmieren:
    - 4 Projektkarten
    - 1 grüne Ereigniskarte
    - 1 rote Ereigniskarte
