# Readme: GRÜN-Lichtenberg

Ein Umweltbildungsspiel für den Berliner Bezirk Lichtenberg *(in Entwicklung)*.

## Kurzbeschreibung

„GRÜN-Lichtenberg“ ist ein kooperatives Umweltbildungspiel für Lichtenberg. Ziel ist es den Wandel hin zu einer nachhaltigen Zukunft für Lichtenberg zu schaffen; d.h. Biodiversität trotz neuer Bedarfe durch Bevölkerungswachstum und Zuzug zu verbessern, eine stabile Grundwasserbilanz zu erreichen und die thermische Belastung zu reduzieren.

Jede Spieler:in ist für 2 bis 3 Stadtteile auf einem der 5 Spielpläne verantwortlich. Jeder Spielplan hat unterschiedliche Ausgangsvorraussetzungen (Mangel und Überschüsse). Für den Spielerfolg wird aber alles zusammengerechnet (Kooperation).

## Spielablauf (Zusammenfassung)

Jede Runde werden zufällige Ereignisse von einem Kartenstapel gezogen (Ereignisphase). Je nach aktuellem Stand des Wandels werden unterschiedliche viele Erfolge oder Krisen gezogen, die die darauffolgende Umsetzungsphase beeinflussen. In der Planungsphase ziehen alle Spieler:innen verdeckt 4 Projektkarten, von denen soviele behalten werden können, wie Planungsbudget vorhanden ist (1 pro Karte). In der Umsetzungsphase können nun der Reihe nach die geplanten Projekte umgesetzt werden bis das Planungsbudget aufgebraucht ist. Ist das der Fall, dann folgt die Wertungssphase in der alle Spielsteine (Vegetation, Freiflächen, Wohnviertel, Energiequellen, Mobilität) gezählt werden um die Marker (Versiegelung, Vegetation, Grundwasser, thermische Belastung und Biodiversität) auf dem Spielbrett "Wandel" entsprechend der Berechnungsregeln zu verschieben. Ist das Spiel dann noch nicht verloren oder gewonnen, beginnt die nächste Runde wieder mit der Ereignisphase.

Während der Ereignis- und Umsetzungsphase gibt es abhängig von den Ereignissen und Projekten immer wieder den Bedarf für Diskussionen und Kooperationen zwischen den Spieler:innen.

Nach spätestens 6 Runden (im Jahr 2095) ist das Spiel vorbei. Konntet ihr Lichtenberg in eine grüne Zukunft bringen?

## Mitwirkung

Bei der Entwicklung von „GRÜN-Lichtenberg“ erhalte ich Unterstützung vom Umweltbüro Lichtenberg, aber auch du kannst mitwirken. Habe ich dein Interesse geweckt? Kontaktiere mich [hier](https://gitlab.com/poinck/lichtenberg/-/issues/new?issue%5Bmilestone_id%5D=) (GitLab-Account notwendig).

